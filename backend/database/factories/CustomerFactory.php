<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Entity\Customer;
use Faker\Generator as Faker;

$factory->define(Customer::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'avatar' => $faker->imageUrl(256, 256),
    ];
});
