<?php

declare(strict_types=1);

namespace App\Http\Request\Customer;

use App\Http\Request\ApiFormRequest;

final class AddCustomerHttpRequest extends ApiFormRequest
{
    public function rules(): array
    {
        return [
            'name' => 'required',
            'email' => 'required|min:5',
            'image' => 'nullable|sometimes|image|mimes:jpeg,png',
        ];
    }
}
